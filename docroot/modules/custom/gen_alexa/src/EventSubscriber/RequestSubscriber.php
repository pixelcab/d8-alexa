<?php

namespace Drupal\gen_alexa\EventSubscriber;

use Drupal\alexa\AlexaEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\views\Views;

/**
 * Class RequestSubscriber.
 *
 * @package Drupal\nec_alexa
 */
class RequestSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    $events['alexaevent.request'][] = array('onRequest', 0);
    return $events;
  }

  public function onRequest(AlexaEvent $event) {
    $request = $event->getRequest();
    $response = $event->getResponse();

    if (!isset($request->intentName)) {
      $request->intentName = '';
    }

    $responseOneLiners = [
      'What\'s up',
      'Hello',
      'Hey there',
      'What it is',
      'You rang',
      'What is it',
      'Yes',
    ];
    $respIndex = mt_rand(0, count($responseOneLiners) - 1);

    switch ($request->intentName) {
      case "greetTeam":
        $speechOutput = 'Hello Genuine PHP Team!';
        $response->respond($speechOutput);
        break;

      case "greetTeamMember":
        $teamMember = $request->slots['Member'];
        if (strtoupper($teamMember) == 'MIKE') {
          $response->respond('Hmmm, i\'m not sure which mike you\'re refering too.')->reprompt('Miles or Dawson?');
        }
        else {
          $speechOutput = $responseOneLiners[$respIndex] . ' ' . $teamMember;
          $response->respond($speechOutput);
        }
        break;

      case 'listEvents':
        $requested_date = $request->slots['Date'];
        $timestamp = date("U",strtotime($requested_date));
        $date_convert = format_date($timestamp, 'custom', 'Ymd');
        $view = Views::getView('event_calendar');
        $view->setDisplay('embed_1');
        $view->setArguments([$date_convert]);
        $render = $view->render();
        $html = drupal_render($render);
        $response->respond(trim($html));
        break;

      case 'AMAZON.HelpIntent':
        $response->respond('For this demo you can ask me to say "hello" to someone on the team or About upcoming events.');
        break;

      default:
        $response->respond('You have reached the Genuine PHP Alexa demo');
        break;
    }
  }
}
